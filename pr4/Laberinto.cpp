#include "Laberinto.h"

#include <iostream>
#include <stdio.h> 

Laberinto::Laberinto(int tama, std::string nLab) {
	tamLab = tama;
	nomLaberinto = nLab;

	matrizLaberinto.clear();
	
	matrizSolucion.clear();
	

	//lectura fichero
	std::cout << "Leyendo fichero \n";
	std::ifstream fichero;
	std::string linea;
	fichero.open(nomLaberinto.c_str());
	if (fichero.good()) {

		bool fin = false;
		
			getline(fichero, linea);
			
			
			if (!fichero.eof()) {
				int i = 0;
				while (linea != "#") {
					std::vector<int> s;
					
					
					splitS(linea, s);
					matrizLaberinto.push_back(s);

					i++;
					getline(fichero, linea);

				}
				for (i = 0; i < tamLab; i++) {
					getline(fichero, linea);
					std::vector<int> s2;
					splitS(linea, s2);
					matrizSolucion.push_back(s2);
				}



			}
			else {
				fin = true;
			}

		fichero.close();
		std::cout << "Fichero leido \n";
	}
	else
	{
		throw std::invalid_argument("El fichero no se puede abrir o est danado");
	}
	// cierra lectura fichero

	for (int i = 0; i < tamLab; i++) {
		for (int j = 0; j < tamLab; j++) {
			std::cout<<matrizLaberinto[i][j]<< " " ;
		}
		std::cout << std::endl;
	}



} //Por defecto, crea valores e inicializa el modelo 3d
Laberinto::Laberinto(Laberinto& l) {

	this->tamLab = l.tamLab;
	this->nomLaberinto = l.nomLaberinto;

	matrizLaberinto.clear();
	matrizSolucion.clear();

	this->matrizLaberinto = l.matrizLaberinto;
	this->matrizSolucion = l.matrizSolucion;



}//Copia
Laberinto::~Laberinto() {

	matrizLaberinto.clear();
	matrizSolucion.clear();



}//Delete

void Laberinto::rendirse() {



} //Metodo que llamar� a la IA en caso de rendirse
void Laberinto::vistaPajaro() {


} // pues eso

void Laberinto::splitS(std::string texto, std::vector<int> &v) {
	//int i = 0;
	char* token;
	const char *textochar = texto.c_str();
	int aux;
	 char delim[] = " ";
	char* next_token;
	token = strtok_s((char*) textochar,delim, &next_token);
	while (token) {
		int aux = 0;
		sscanf_s(token, "%d\n", &aux);
		//aux = *token - '0' ;
		v.push_back(aux);
		token = strtok_s(NULL, delim, &next_token);
		//i++;
	}
}