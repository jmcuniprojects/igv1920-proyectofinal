#include <cstdlib>
#include <stdio.h>

#include "igvEscena3D.h"
#include "igvFuenteLuz.h"
#include "igvMaterial.h"
#include "igvTextura.h"
#include <list>

// Metodos constructores 

igvEscena3D::igvEscena3D () {
}

igvEscena3D::~igvEscena3D() {
}


// Metodos publicos 

void pintar_ejes(void) {
  GLfloat rojo[]={1,0,0,1.0};
  GLfloat verde[]={0,1,0,1.0};
  GLfloat azul[]={0,0,1,1.0};

  glMaterialfv(GL_FRONT,GL_EMISSION,rojo);
	glBegin(GL_LINES);
		glVertex3f(1000,0,0);
		glVertex3f(-1000,0,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,verde);
	glBegin(GL_LINES);
		glVertex3f(0,1000,0);
		glVertex3f(0,-1000,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,azul);
	glBegin(GL_LINES);
		glVertex3f(0,0,1000);
		glVertex3f(0,0,-1000);
	glEnd();
}


void pintar_quad(float div_x, float div_z) {
	float ini_x = 0.0;
	float ini_z = 0.0;
	float tam_x = 5.0/div_x;
	float tam_z = 5.0 / div_z;
		
	glNormal3f(0,1,0);
	glBegin(GL_QUADS);
	while (ini_z < 5.0) {
		ini_x = 0.0;
		while (ini_x < 5.0) {
			glTexCoord2f(ini_x / 5, 1 - ini_z / 5);
			glVertex3f(ini_x, 0.0, ini_z);
			glTexCoord2f(ini_x / 5, 1 - (ini_z + tam_z) / 5);
			glVertex3f(ini_x, 0.0, ini_z + tam_z);
			glTexCoord2f((ini_x + tam_x) / 5, 1 - (ini_z + tam_z) / 5);
			glVertex3f(ini_x + tam_x, 0.0, ini_z + tam_z);
			glTexCoord2f((ini_x + tam_x) / 5, 1 - ini_z / 5);
			glVertex3f(ini_x + tam_x, 0.0, ini_z);
			ini_x += tam_x;
		}
		ini_z += tam_z;
	}
	glEnd();
}


void igvEscena3D::visualizar(void) {
	// crear el modelo
	glPushMatrix(); // guarda la matriz de modelado

		// se pintan los ejes
	  if (ejes) pintar_ejes();

	glPopMatrix (); // restaura la matriz de modelado
}
