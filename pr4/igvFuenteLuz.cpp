#include "igvFuenteLuz.h"

// Metodos constructores 

igvFuenteLuz::igvFuenteLuz() {
}

igvFuenteLuz::igvFuenteLuz(const unsigned int _idLuz,
													 const igvPunto3D  & _posicion,
													 const igvColor & cAmb, const igvColor & cDif, const igvColor & cEsp,
													 const double a0, const double a1, const double a2) {

	idLuz = _idLuz;
														 
	posicion = _posicion;
														 
	colorAmbiente = cAmb;
	colorDifuso = cDif;
	colorEspecular = cEsp;

	aten_a0 = a0;
	aten_a1 = a1;
	aten_a2 = a2;

	// valores por defecto cuando la luz no es un foco
	direccion_foco = igvPunto3D(0,0,0);
	angulo_foco = 180; // de esta manera la luz se convierte en puntual	
	exponente_foco = 0;

	encendida = true;
}

igvFuenteLuz::igvFuenteLuz(const unsigned int _idLuz,
			                     const igvPunto3D & _posicion, 
			                     const igvColor& cAmb, const igvColor& cDif, const igvColor& cEsp,
								           const double a0, const double a1, const double a2,
													 const igvPunto3D& dir_foco, const double ang_foco, const double exp_foco) {

	idLuz = _idLuz;
														 
	posicion = _posicion;
														 
	colorAmbiente = cAmb;
	colorDifuso = cDif;
	colorEspecular = cEsp;

	aten_a0 = a0;
	aten_a1 = a1;
	aten_a2 = a2;

	direccion_foco = dir_foco;
	angulo_foco = ang_foco;
	exponente_foco = exp_foco;

	encendida = true;
}

// Metodos publicos ----------------------------------------

igvPunto3D& igvFuenteLuz::getPosicion(void) {
	return posicion;
}

void igvFuenteLuz::setPosicion(igvPunto3D pos) {
	posicion = pos;
}


void igvFuenteLuz::set(const igvColor & cAmb, const igvColor & cDif, const igvColor & cEsp) {
	colorAmbiente = cAmb;
	colorDifuso = cDif;
	colorEspecular = cEsp;
}

void igvFuenteLuz::setAmbiental(const igvColor & cAmb) {
	colorAmbiente = cAmb;
}

void igvFuenteLuz::setDifuso(const igvColor & cDif) {
	colorDifuso = cDif;
}

void igvFuenteLuz::setEspecular(const igvColor & cEsp) {
	colorEspecular = cEsp;
}

igvColor & igvFuenteLuz::getAmbiental(void) {
	return colorAmbiente;
}

igvColor & igvFuenteLuz::getDifuso(void) {
	return colorDifuso;
}

igvColor & igvFuenteLuz::getEspecular(void) {
	return colorEspecular;
}

void igvFuenteLuz::setAtenuacion(double a0, double a1, double a2) {
	aten_a0 = a0;
	aten_a1 = a1;
	aten_a2 = a2;
}

void igvFuenteLuz::getAtenuacion(double & a0, double & a1, double & a2) {
	a0 = aten_a0;
	a1 = aten_a1;
	a2 = aten_a2;
}

void igvFuenteLuz::encender(void) {
	encendida = true;
}

void igvFuenteLuz::apagar(void) {
	encendida = false;
}

unsigned int igvFuenteLuz::esta_encendida(void) {
	return encendida;
}

void igvFuenteLuz::aplicar(void) {

// APARTADO A
	if (encendida) { // si la luz est� encendida
		glEnable(idLuz);		//	activar la luz
		GLfloat aux[4] = { (GLfloat)posicion.c[X], (GLfloat)posicion.c[Y], (GLfloat)posicion.c[Z],1 };
		glLightfv(idLuz, GL_POSITION, aux);//	establecer la posici�n de la luz
		//	establecer los colores ambiental, difuso y especular
		GLfloat aux2[4] = { (GLfloat)colorAmbiente[R], (GLfloat)colorAmbiente[G], (GLfloat)colorAmbiente[B], (GLfloat)colorAmbiente[A] };
		glLightfv(idLuz, GL_AMBIENT, aux2);
		GLfloat aux3[4] = { (GLfloat)colorDifuso[R], (GLfloat)colorDifuso[G], (GLfloat)colorDifuso[B], (GLfloat)colorDifuso[A] };
		glLightfv(idLuz, GL_DIFFUSE, aux3);
		GLfloat aux4[4] = { (GLfloat)colorEspecular[R], (GLfloat)colorEspecular[G], (GLfloat)colorEspecular[B], (GLfloat)colorEspecular[A] };
		glLightfv(idLuz, GL_SPECULAR, aux4);
		//	establecer la atenuaci�n radial
		GLfloat aux5[3] = { (GLfloat)aten_a0, (GLfloat)aten_a1, (GLfloat)aten_a2 };
		glLightf(idLuz, GL_CONSTANT_ATTENUATION, aux5[0]);
		glLightf(idLuz, GL_LINEAR_ATTENUATION, aux5[1]);
		glLightf(idLuz, GL_QUADRATIC_ATTENUATION, aux5[2]);
		//	establecer la atenuaci�n angular y la direcci�n del foco
		GLfloat aux6[3] = { (GLfloat)direccion_foco.c[X], (GLfloat)direccion_foco.c[Y], (GLfloat)direccion_foco.c[Z] };
		glLightfv(idLuz, GL_SPOT_DIRECTION, aux6);
		GLfloat aux7[2] = { (GLfloat)angulo_foco, (GLfloat)exponente_foco };
		glLightf(idLuz, GL_SPOT_CUTOFF, aux7[0]);
		glLightf(idLuz, GL_SPOT_EXPONENT, aux7[1]);
	}
	else {// si la luz est� apagada
		glDisable(idLuz);		//	desactivar la luz
	}

	

}

