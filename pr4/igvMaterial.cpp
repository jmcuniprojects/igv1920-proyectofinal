#include "igvMaterial.h"

// Metodos constructores 

igvMaterial::igvMaterial () {
	
}

igvMaterial::~igvMaterial () {

}

igvMaterial::igvMaterial (const igvMaterial& p) {	//de copia
	Ka = p.Ka;
	Kd = p.Kd;
	Ks = p.Ks;
	Ns = p.Ns;
}

igvMaterial::igvMaterial(igvColor _Ka, igvColor _Kd, igvColor _Ks, double _Ns) {
	Ka = _Ka;
	Kd = _Kd;
	Ks = _Ks;
	Ns = _Ns;
}

// Metodos publicos 

void igvMaterial::aplicar(void) {

// APARTADO C
// Aplicar los valores de los atributos del objeto igvMaterial:
// - coeficiente de reflexi�n de la luz ambiental
	GLfloat cofAmb[4] = { Ka[R],Ka[G],Ka[B],Ka[A] };
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, cofAmb);
// - coeficiente de reflexi�n difuso
	GLfloat cofDif[4] = { Kd[R],Kd[G],Kd[B],Kd[A] };
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, cofDif);
// - coeficiente de reflesi�n especular
	GLfloat cofEsp[4] = { Ks[R],Ks[G],Ks[B],Ks[A] };
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, cofEsp);
// - exponente de Phong
	GLfloat expPh = Ns;
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, expPh);

// establecer como color de emisi�n (0.0, 0.0, 0.0) (el objeto no es una fuente de luz)
	GLfloat emm[4] = { 0.0,0.0,0.0,0.0 };
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emm);


}

void igvMaterial::set(igvColor _Ka, igvColor _Kd, igvColor _Ks, double _Ns) {
	Ka = _Ka;
	Kd = _Kd;
	Ks = _Ks;
	Ns = _Ns;
}



