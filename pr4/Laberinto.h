#ifndef __LABERINTO
#define __LABERINTO

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>


class Laberinto
{
	public:
		Laberinto(int tama, std::string nLab); //Por defecto, crea valores e inicializa el modelo 3d
		Laberinto(Laberinto& l); //Copia
		~Laberinto(); //Delete

		void rendirse(); //Metodo que llamar� a la IA en caso de rendirse
		void vistaPajaro(); // pues eso

		void setTamLab(int t) {
			tamLab = t;
		}

		void setNomLab(std::string s) {
			nomLaberinto = s;
		}

		int getTam() {
			return tamLab;
		}

		std::string getNomLab() {
			return nomLaberinto;
		}


	private:
		int tamLab; //Tama�o de Laberinto
		std::string nomLaberinto; //Nombre del laberinto, para coger el fichero y cargarlo
		std::vector<std::vector<int>> matrizLaberinto; //Matriz que ver� el jugador
		std::vector<std::vector<int>> matrizSolucion; //Solucion de la Matriz para la IA
		void splitS( std::string texto, std::vector<int> &v );
};

#endif

